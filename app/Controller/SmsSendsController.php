<?php

App::uses('AppController', 'Controller');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SmsSendsController extends AppController {

    public $uses = array('DailyCron', 'Setting', 'SmsSend', 'SmsLog', 'SmsFeedback', 'SmsResponse','ExceptionLogger', 'SmsException');
    
    public function send($date = null) {
        $time_start = microtime(true);
        if ($date == null) {
            $date = date('Y-m-d');
        }
        $dailycrons = $this->DailyCron->find('count', array(
            'conditions' => array('AND' => array(
                    'DailyCron.created LIKE' => "$date%",
                ))
        ));
        if ($dailycrons > 0) {
            //exit;
        }

        $setting = $this->Setting->find('first');
        $weburl = $setting['Setting']['weburl'];
        $filename = 'sms-' . $date . '.csv';
        $file = $weburl . '/files/cron/' . $date . '/' . $filename;

        $dailyCronArray = array(
            'file_name' => $filename,
            'is_file' => 2
        );
        $this->DailyCron->save($dailyCronArray);
        $lastcronid = $this->DailyCron->getInsertID();
        $smsFile = fopen($file, 'r');
        $i = 0;
        while (!feof($smsFile)) {
           /*
            if ($i == 0) {
                $headings = fgetcsv($smsFile);
                echo '<pre>';
                print_r($headings);
                exit;
            } else {
            * 
            */
                $row = fgetcsv($smsFile);
                /* Make a function to clean phone number and get last ten digits */
                $phone_number = $this->SmsSend->clean_phone($row[3]);
                /* Make a function to clean phone number and get last ten digits */
                $sms_feedback[] = array(
                    'survey_id' => $row[0],
                    'consumer_id' => $row[1],
                    'consumer_name' => $row[2],
                    'consumer_phone' => '0'.$phone_number,
                    'survey_feedback_id' => $row[4],
                    'message' => $row[5],
                    'question_id' => $row[6]
                );

                $message = $row[5];
                $ConsumerName = '';
                if(strlen($row[2]) > 18){
                    $ConsumerFullName = explode(" ", $row[2]);
                    if(isset($ConsumerFullName[0]) && !empty($ConsumerFullName[0]) && isset($ConsumerFullName[1]) && !empty($ConsumerFullName[1]) && strlen($ConsumerFullName[0].' '.$ConsumerFullName[1]) <= 18){
                        $ConsumerName = $ConsumerFullName[0].' '.$ConsumerFullName[1];
                    }else{
                        $ConsumerName = $ConsumerFullName[0];
                    }
                }else{
                    $ConsumerName = $row[2];
                }
                $messageText = 'AoA, '.$ConsumerName."\n".$message;
                
                
                $res = $this->SmsSend->sendSMS('0'.$phone_number, urlencode($messageText));    
                
                $response = explode(' ',$res);
                if($response[0]!=200){
                    $SmsExceptionData = array(
                        'consumer_phone' =>  '0'.$phone_number,
                        'survey_feedback_id' => $row[4],
                        'survey_id' => $row[0],
                        'exception' => $res
                    );
                    $this->SmsException->save($SmsExceptionData);
                }
                $smsLogArray[] = array(
                    'phone_number' => '0'.$phone_number, // Add +92 or 0 as prefix
                    'message' => $message,
                    'is_sent' => 1,
                    'status' => $response[0]
                );
            //}
            $i++;
        }
        fclose($smsFile);

        $newSmsfeedback = array();
        $j = 0;
        foreach ($sms_feedback as $smskey => $smsvalue) {
            if (!empty($smsvalue['survey_id']) && !empty($smsvalue['consumer_id']) && !empty($smsvalue['consumer_name']) && !empty($smsvalue['consumer_phone']) && !empty($smsvalue['survey_feedback_id']) && !empty($smsvalue['message'])) {
                $newSmsfeedback[$j] = $smsvalue;
                $j++;
            }
        }
        $this->SmsFeedback->saveAll($newSmsfeedback);

        $newSmsLogArray = array();
        $i = 0;
        foreach ($smsLogArray as $key => $value) {
            if (!empty($value['phone_number']) && !empty($value['message']) && !empty($value['is_sent'])) {
                $newSmsLogArray[$i] = $value;
                $i++;
            }
        }
        $this->SmsLog->saveAll($newSmsLogArray);

        $dailyCronArray2 = array(
            'id' => $lastcronid,
            'is_file' => 1
        );
		// use clear
        $this->DailyCron->save($dailyCronArray2);
        $time_end = microtime(true);
        // Function in another class or appcontroller
		$execution_time = $time_end - $time_start;
        if ($execution_time > 30) {
            App::uses('CakeEmail', 'Network/Email');
            $email = new CakeEmail();
            $email->config('gmail');
            $email->from(array('me@example.com' => __('Please Do Not Reply')));
            $email->to(array('mazhar.abbas376@gmail.com', 'shehroze45@gmail.com'));
            $email->subject('Execution time');
            $body = 'Execution time of sms send() is : ' . $execution_time . ' seconds';
            $email->send($body);
        }
    }
	

    Public function responsesms($phone = null, $smsResponse = null) {
        
		/* Shehroze Code */
        /*
        $data = $this->request->query;
        $phone = $data['from'];
        $smsResponse = $data['message'];
        $smsResponseArray = array(
                'phone_number' => $phone,
                'response' => $smsResponse
            );
            $this->SmsResponse->save($smsResponseArray);
        exit();
		/* Shehroze Code */
		
	// Get last ten digits of phone and than query it.
        $phone = substr($phone, -10);
        
        $smsfeedback = $this->SmsFeedback->find('first', array(
            'conditions' => array('AND' => array(
                    'SmsFeedback.consumer_phone' => '0'.$phone
                )),
            'order' => 'SmsFeedback.created DESC'
        ));
        
        if (count($smsfeedback) == 0) {
            $exceptionLogger = array(
                'phone' => '0'.$phone,
                'action' => 'responseSMS',
                'controller' => 'SmsSendsController',
                'message' => 'This phone number Does not exists'
            );
            $this->ExceptionLogger->save($exceptionLogger);
            App::uses('CakeEmail', 'Network/Email');
            $email = new CakeEmail();
            $email->config('gmail');
            $email->from(array('me@example.com' => __('Please Do Not Reply')));
            $email->to(array('mazhar.abbas376@gmail.com', 'shehroze45@gmail.com'));
            $email->subject('Execution time');
            $body = $exceptionLogger;
            $email->send($body);
            exit;
        } else {
            $this->SmsFeedback->id = $smsfeedback['SmsFeedback']['id'];
            $smsFeedbackArray = array(
                'is_responded' => 1,
                'response' => $smsResponse
            );
            $this->SmsFeedback->save($smsFeedbackArray);

            $smsResponseArray = array(
                'phone_number' => '0'.$phone,
                'response' => $smsResponse
            );
            $this->SmsResponse->save($smsResponseArray);

            $setting = $this->Setting->find('first');
            $base_url = $setting['Setting']['service1'];

            $post = array(
                'id' => $smsfeedback['SmsFeedback']['survey_feedback_id'],
                'response' => $smsResponse,
                'secretcode' => 'A000B10',
		'status' => 'SUCCESS'
            );
            
            $url = $base_url;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));

            $str = curl_exec($curl);
            curl_close($curl);
            var_dump($str);
        }
    }

}
