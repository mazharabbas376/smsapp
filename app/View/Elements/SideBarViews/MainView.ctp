<div class="tab-pane active" id="side-tab-menu">
    <!-- Primary Navigation -->
    <nav id="primary-nav">
        <ul>
            <li>
                <a href="#" class=" active"><i class="glyphicon-display"></i>Dashboard</a>
            </li>
            <li>
                <a href="#" class="menu-link"><i class="glyphicon-vector_path_circle"></i>Send SMS</a>
                <ul>
                    <li>
                        <a href="<?php echo $this->Html->url('/sms_sends/send', true); ?>">Send</a>
                    </li>
                    
                </ul>
            </li>
        </ul>
    </nav>
    <!-- END Primary Navigation -->
</div>