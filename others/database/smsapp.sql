-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2015 at 06:44 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smsapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `daily_crons`
--

CREATE TABLE IF NOT EXISTS `daily_crons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(55) NOT NULL,
  `is_file` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exception_loggers`
--

CREATE TABLE IF NOT EXISTS `exception_loggers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(255) NOT NULL,
  `response` varchar(255) NOT NULL,
  `exception` varchar(255) NOT NULL,
  `message` text,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weburl` text NOT NULL,
  `service1` text NOT NULL,
  `service2` text NOT NULL,
  `service3` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `weburl`, `service1`, `service2`, `service3`, `created`, `modified`) VALUES
(1, 'http://localhost/phednew', 'http://localhost/phednew/response_services/smsResponseService', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sms_feedbacks`
--

CREATE TABLE IF NOT EXISTS `sms_feedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `consumer_id` int(11) NOT NULL,
  `consumer_name` varchar(55) NOT NULL,
  `consumer_phone` varchar(55) NOT NULL,
  `survey_feedback_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `question_id` int(11) NOT NULL,
  `is_responded` tinyint(4) NOT NULL DEFAULT '0',
  `response` text,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sms_logs`
--

CREATE TABLE IF NOT EXISTS `sms_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(55) NOT NULL,
  `message` text NOT NULL,
  `is_sent` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sms_responses`
--

CREATE TABLE IF NOT EXISTS `sms_responses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(55) NOT NULL,
  `response` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sms_sends`
--

CREATE TABLE IF NOT EXISTS `sms_sends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(55) NOT NULL,
  `message` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
